﻿using BinaryTree.Painter;
using BinaryTree.QueryMenu;
using BinaryTree.SearchMenu;
using BinaryTree.TraversalMenu;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryTree
{
	public partial class FormMain : Form
	{
		private SearchTree tree = new BinarySearchTree.BinarySearchTree();
		private Dictionary<int, Color> searchResultList = new Dictionary<int, Color>();

		public FormMain()
		{
			InitializeComponent();
			tree.AddRange(new List<int>() { 8, 4, 12, 2, 6, 10, 14, 1, 3, 5, 7, 11, 13 });
		}

		private void FormMain_Load(object sender, EventArgs e)
		{
			Init();
		}

		private void DrawTree(Graphics g)
		{
			g.TranslateTransform(1.0f, 1.0f);
			SearchTreePainter p = new SearchTreePainter(tree, panelTree.Width, panelTree.Height, searchResultList);
			p.Draw(g);
		}

		private void panelTree_Paint(object sender, PaintEventArgs e)
		{
			DrawTree(e.Graphics);
		}

		private void panelTree_SizeChanged(object sender, EventArgs e)
		{
			panelTree.Invalidate();
		}

		private void btnAddKey_Click(object sender, EventArgs e)
		{
			var formInput = new FormInput("Nhập giá trị cần thêm");
			formInput.ShowDialog();
			if (formInput.Submited)
			{
				tree.Add(formInput.Value);
				panelTree.Invalidate();
			}
		}

		private void btnDeleteKey_Click(object sender, EventArgs e)
		{
			var formInput = new FormInput("Nhập giá trị cần xóa");
			formInput.ShowDialog();
			if (formInput.Submited)
			{
				tree.Remove(formInput.Value);
				panelTree.Invalidate();
			}
		}

		public void ResetSearchResultList()
		{
			searchResultList = new Dictionary<int, Color>();
		}

		public void AddSearchResult(int key, Color value)
		{
			searchResultList.Add(key, value);
		}

		public void ReDraw()
		{
			panelTree.Invalidate();
		}

		List<SearchMenu.SearchMenu> searchMenuList;
		List<QueryMenu.QueryMenu> queryMenuList;
		List<TraversalMenu.TraversalMenu> traversalMenuList;

		public void Init()
		{
			searchMenuList = new List<SearchMenu.SearchMenu>();
			searchMenuList.Add(new SearchByKeyMenu(this.tree, FormMainSearchProcessor.GetInstance(this)));
			searchMenuList.Add(new SearchMinValueMenu(this.tree, FormMainSearchProcessor.GetInstance(this)));
			searchMenuList.Add(new SearchMaxValueMenu(this.tree, FormMainSearchProcessor.GetInstance(this)));
			searchMenuList.Add(new SearchRightTreeMinValueMenu(this.tree, FormMainSearchProcessor.GetInstance(this)));
			searchMenuList.Add(new SearchLeftTreeMaxValueMenu(this.tree, FormMainSearchProcessor.GetInstance(this)));


			foreach (SearchMenu.SearchMenu m in searchMenuList)
			{
				ddSearch.DropDownItems.Add(m.ToolStripMenu);
			}

			queryMenuList = new List<QueryMenu.QueryMenu>();
			queryMenuList.Add(new CountLeapNodesMenu(this.tree));
			queryMenuList.Add(new OnlyHaveOneChildTreeNodesMenu(this.tree));
			queryMenuList.Add(new OnlyHaveRightChildTreeNodesMenu(this.tree));
			queryMenuList.Add(new OnlyHaveLeftChildTreeNodesMenu(this.tree));
			queryMenuList.Add(new HaveTwoChildTreesNodesMenu(this.tree));
			queryMenuList.Add(new TreeHeightMenu(this.tree));
			queryMenuList.Add(new CountNodesMenu(this.tree));
			queryMenuList.Add(new CountNodesByLevelMenu(this.tree));
			queryMenuList.Add(new DirectionMenu(this.tree));

			foreach (QueryMenu.QueryMenu m in queryMenuList)
			{
				ddQuery.DropDownItems.Add(m.ToolStripMenu);
			}

			traversalMenuList = new List<TraversalMenu.TraversalMenu>();
			traversalMenuList.Add(new TraversalLeftNodeRightMenu(this.tree));
			traversalMenuList.Add(new TraversalNodeLeftRightMenu(this.tree));
			traversalMenuList.Add(new TraversalLeftRightNodeMenu(this.tree));

			foreach (TraversalMenu.TraversalMenu m in traversalMenuList)
			{
				ddTraversal.DropDownItems.Add(m.ToolStripMenu);
			}
		}
	}
}
