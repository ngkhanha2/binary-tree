﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.BinarySearchTree
{
	class BinarySearchTreeNode : SearchTreeNode
	{
		private InternalNode _node;

		protected BinarySearchTreeNode(InternalNode node)
		{
			this._node = node;
		}

		public override SearchTreeNode Left
		{
			get
			{
				return BinarySearchTreeNode.GetNode(this._node.Left);
			}
		}

		public override SearchTreeNode Right
		{
			get
			{
				return BinarySearchTreeNode.GetNode(this._node.Right);
			}
		}

		public override int Value
		{
			get
			{
				return this._node.Key;
			}
		}

		public static BinarySearchTreeNode GetNode(InternalNode node)
		{
			if (node == null)
			{
				return null;
			}
			return new BinarySearchTreeNode(node);
		}
	}
}
