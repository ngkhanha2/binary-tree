﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.BinarySearchTree
{
	class InternalNode
	{
		private int _key;

		public InternalNode(int key)
		{
			this._key = key;
		}

		private InternalNode _left;

		private InternalNode _right;

		public InternalNode Left
		{
			get
			{
				return _left;
			}

			set
			{
				_left = value;
			}
		}

		public InternalNode Right
		{
			get
			{
				return _right;
			}

			set
			{
				_right = value;
			}
		}

		public int Key
		{
			get
			{
				return _key;
			}

			set
			{
				_key = value;
			}
		}
	}
}
