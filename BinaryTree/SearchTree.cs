﻿using BinaryTree.QueryFunction;
using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
	public interface SearchTree
	{
		void Add(int key);

		void AddRange(List<int> keys);

		void Remove(int key);

		SearchResult Search(ISearchFunction function);

		QueryResult Query(IQueryFunction function);

		List<SearchTreeNode> Travel(Traversal.Traversal traversal);

		SearchTreeNode GetRoot();
	}
}
