﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class CountNodesMenu : QueryMenu
	{
		public CountNodesMenu(SearchTree tree) : base(tree, "Đếm số nút trên cây")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Query(new CountNodes());
				string content = "Số nút cây: " + ((int)result.Value).ToString();
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
