﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class HaveTwoChildTreesNodesMenu : QueryMenu
	{
		public HaveTwoChildTreesNodesMenu(SearchTree tree) : base(tree, "Đếm số nút có hai cây con")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Query(new HaveTwoChildTreesNodes());
				string content = "Số nút có hai cây con: " + ((int)result.Value).ToString();
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
