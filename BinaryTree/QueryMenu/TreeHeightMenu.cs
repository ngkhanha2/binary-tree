﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class TreeHeightMenu : QueryMenu
	{
		public TreeHeightMenu(SearchTree tree) : base(tree, "Chiều cao cây")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Query(new TreeHeight());
				string content = "Chiều cao cây: " + ((int)result.Value).ToString();
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
