﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class OnlyHaveOneChildTreeNodesMenu : QueryMenu
	{
		public OnlyHaveOneChildTreeNodesMenu(SearchTree tree) : base(tree, "Đếm số nút chỉ có một cây con")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Query(new OnlyHaveOneChildTreeNodes());
				string content = "Số nút chỉ có một cây con: " + ((int)result.Value).ToString();
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
