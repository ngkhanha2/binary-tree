﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class DirectionMenu : QueryMenu
	{
		public DirectionMenu(SearchTree tree) : base(tree, "Xác định độ dài đường đi từ nút gốc đến nút có giá trị là x")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var form = new FormInput("Nhập giá trị cần tìm");
				form.ShowDialog();
				if (form.Submited)
				{
					var result = this.Tree.Query(new Direction(form.Value));
					string content = "Xác định độ dài đường đi từ nút gốc đến nút có giá trị là " + form.Value + ": " + ((int)result.Value).ToString();
					var resultForm = new FormQueryResult(content);
					resultForm.ShowDialog();
				}
			}
		}
	}
}
