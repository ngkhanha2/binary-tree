﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class OnlyHaveLeftChildTreeNodesMenu : QueryMenu
	{
		public OnlyHaveLeftChildTreeNodesMenu(SearchTree tree) : base(tree, "Đếm số nút chỉ có một cây con trái")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Query(new OnlyHaveLeftChildTreeNodes());
				string content = "Số nút chỉ có một cây con trái: " + ((int)result.Value).ToString();
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
