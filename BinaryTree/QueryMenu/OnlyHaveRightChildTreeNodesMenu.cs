﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class OnlyHaveRightChildTreeNodesMenu : QueryMenu
	{
		public OnlyHaveRightChildTreeNodesMenu(SearchTree tree) : base(tree, "Đếm số nút chỉ có một cây con phải")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Query(new OnlyHaveRightChildTreeNodes());
				string content = "Số nút chỉ có một cây con phải: " + ((int)result.Value).ToString();
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
