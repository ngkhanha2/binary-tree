﻿using BinaryTree.QueryFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryMenu
{
	public class CountNodesByLevelMenu : QueryMenu
	{
		public CountNodesByLevelMenu(SearchTree tree) : base(tree, "Đếm số nút trên từng mức của cây")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var form = new FormInput("Nhập mức cần truy vấn");
				form.ShowDialog();
				if (form.Submited)
				{
					var result = this.Tree.Query(new CountNodesByLevel(form.Value));
					string content = "Số nút trên mức " + form.Value.ToString() + ": " + ((int)result.Value).ToString();
					var resultForm = new FormQueryResult(content);
					resultForm.ShowDialog();
				}
			}
		}
	}
}
