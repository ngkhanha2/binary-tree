﻿using BinaryTree.Traversal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.TraversalMenu
{
	public class TraversalLeftRightNodeMenu : TraversalMenu
	{
		public TraversalLeftRightNodeMenu(SearchTree tree) : base(tree, "Left - Right - Node")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Travel(new LeftRightNode());
				string content = "Thứ tự duyệt theo phương pháp Left - Right - Node:\n";
				foreach (SearchTreeNode n in result)
				{
					content += n.Value.ToString() + "\n";
				}
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
