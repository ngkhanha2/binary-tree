﻿using BinaryTree.Traversal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.TraversalMenu
{
	public class TraversalLeftNodeRightMenu : TraversalMenu
	{
		public TraversalLeftNodeRightMenu(SearchTree tree) : base(tree, "Left - Node - Right")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Travel(new LeftNodeRight());
				string content = "Thứ tự duyệt theo phương pháp Left - Node - Right:\n";
				foreach (SearchTreeNode n in result)
				{
					content += n.Value.ToString() + "\n";
				}
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
