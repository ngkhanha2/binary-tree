﻿using BinaryTree.Traversal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.TraversalMenu
{
	public class TraversalNodeLeftRightMenu : TraversalMenu
	{
		public TraversalNodeLeftRightMenu(SearchTree tree) : base(tree, "Node - Left - Right")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			if (this.Tree != null)
			{
				var result = this.Tree.Travel(new NodeLeftRight());
				string content = "Thứ tự duyệt theo phương pháp Node - Left - Right:\n";
				foreach (SearchTreeNode n in result)
				{
					content += n.Value.ToString() + "\n";
				}
				var form = new FormQueryResult(content);
				form.ShowDialog();
			}
		}
	}
}
