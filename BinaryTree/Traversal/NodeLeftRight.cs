﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Traversal
{
	public class NodeLeftRight : Traversal
	{
		public override List<SearchTreeNode> Travel(SearchTreeNode node)
		{
			return Process(node);
		}

		private List<SearchTreeNode> Process(SearchTreeNode node)
		{
			List<SearchTreeNode> l = new List<SearchTreeNode>();
			l.Add(node);
			if (node.Left != null)
			{
				var r = Process(node.Left);
				ConcatenateList(l, r);
			}
			if (node.Right != null)
			{
				var r = Process(node.Right);
				ConcatenateList(l, r);
			}
			return l;
		}
	}
}
