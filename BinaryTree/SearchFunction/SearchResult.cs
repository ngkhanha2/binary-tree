﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchFunction
{
	public class SearchResult
	{
		private Object _value;

		public SearchResult(Object obj)
		{
			this._value = obj;
		}

		public object Value
		{
			get
			{
				return _value;
			}
		}
	}
}
