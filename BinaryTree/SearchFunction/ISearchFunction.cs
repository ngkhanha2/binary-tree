﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchFunction
{
	public interface ISearchFunction
	{
		SearchResult Search(SearchTreeNode iterator);
	}
}
