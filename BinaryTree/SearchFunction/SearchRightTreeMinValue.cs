﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchFunction
{
	public class SearchRightTreeMinValue : ISearchFunction
	{
		public SearchResult Search(SearchTreeNode node)
		{
			if (node == null || node.Right == null)
			{
				return null;
			}
			var i = node.Right;
			SearchTreeNode result = null;
			while (i != null)
			{
				result = i;
				i = i.Left;
			}
			return new SearchResult(result);
		}
	}
}
