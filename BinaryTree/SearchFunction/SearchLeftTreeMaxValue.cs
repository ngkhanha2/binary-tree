﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchFunction
{
	public class SearchLeftTreeMaxValue : ISearchFunction
	{
		public SearchResult Search(SearchTreeNode node)
		{
			if (node == null || node.Left == null)
			{
				return null;
			}
			var i = node.Left;
			SearchTreeNode result = null;
			while (i != null)
			{
				result = i;
				i = i.Right;
			}
			return new SearchResult(result);
		}
	}
}
