﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Painter
{
	public class SearchTreePainter : IPainter
	{
		private int nodeSize = 20;
		private float fontSize = 15;
		private int alignTop = 50;

		Pen edgePen = new Pen(Color.Black, 2.0f);
		Pen borderPen = new Pen(Color.Black, 2.0f);
		Brush nodeFill = new SolidBrush(Color.SkyBlue);
		Brush fontBrush = new SolidBrush(Color.Black);


		private int maxTreeHeight, totalNodes;
		private SearchTree _tree;
		private int width, height;
		private SearchTreeNodePainter root;

		private Pen blackPen = new Pen(Color.Black);
		private Brush blackBrush = new SolidBrush(Color.Black);

		private Font font;

		private int XSCALE, YSCALE;
		private Dictionary<int, Color> optionalNodeColors;

		public SearchTreePainter(SearchTree tree, int width, int height, Dictionary<int, Color> optionalNodeColors)
		{
			this._tree = tree;
			this.totalNodes = 0;
			this.width = width - 100;
			this.height = height - 100;
			this.root = InorderTraversal(_tree.GetRoot(), 1);
			this.maxTreeHeight = TreeHeight(root);

			this.totalNodes = totalNodes == 0 ? 2 : totalNodes;
			this.maxTreeHeight = maxTreeHeight < 0 ? 0 : maxTreeHeight;

			XSCALE = this.width / totalNodes; //scale x by total nodes in tree
			YSCALE = this.height / (maxTreeHeight + 1); //scale y by tree height

			this.font = new Font("Consolas", fontSize, FontStyle.Bold);

			this.optionalNodeColors = optionalNodeColors;
		}

		private SearchTreeNodePainter InorderTraversal(SearchTreeNode t, int depth)
		{
			if (t != null)
			{
				var node = new SearchTreeNodePainter();
				node.Left = InorderTraversal(t.Left, depth + 1); //add 1 to depth (y coordinate) 
				node.X = totalNodes + 1;
				node.Y = depth - 1;
				node.Value = t.Value;
				++totalNodes;
				node.Right = InorderTraversal(t.Right, depth + 1);
				return node;
			}
			return null;
		}

		private int TreeHeight(SearchTreeNodePainter t)
		{
			if (t == null) return -1;
			else return 1 + Math.Max(TreeHeight(t.Left), TreeHeight(t.Right));
		}

		public void Draw(Graphics g)
		{
			DrawNode(this.root, g);
		}

		private void DrawNode(SearchTreeNodePainter root, Graphics g)
		{
			int dx = 0, dy = 0, dx2 = 0, dy2 = 0;
			if (root != null)
			{
				dx = root.X * XSCALE; // get x,y coords., and scale them 
				dy = alignTop + root.Y * YSCALE;

				// this draws the lines from a node to its children, if any
				if (root.Left != null)
				{
					//draws the line to left child if it exists
					dx2 = root.Left.X * XSCALE;
					dy2 = alignTop + root.Left.Y * YSCALE;
					g.DrawLine(blackPen, dx, dy, dx2, dy2);
				}

				DrawNode(root.Left, g);

				if (root.Right != null)
				{
					//draws the line to right child if it exists
					dx2 = root.Right.X * XSCALE; //get right child x,y scaled position
					dy2 = alignTop + root.Right.Y * YSCALE;
					g.DrawLine(blackPen, dx, dy, dx2, dy2);
				}

				DrawNode(root.Right, g);

				string s = root.Value.ToString(); //get the word at this node

				RectangleF nodeRect = new RectangleF(dx - nodeSize / 2, dy - nodeSize / 2, nodeSize, nodeSize);

				if (this.optionalNodeColors.ContainsKey(root.Value))
				{
					g.FillEllipse(new SolidBrush(optionalNodeColors[root.Value]), nodeRect);
				}
				else
				{
					g.FillEllipse(nodeFill, nodeRect);
				}
				g.DrawEllipse(borderPen, nodeRect);

				g.DrawString(s, font, blackBrush, new PointF(dx + 20, dy - fontSize * 2 / 3));

			}
		}
	}
}
