﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Painter
{
	class SearchTreeNodePainter
	{
		private SearchTreeNodePainter _left;
		private SearchTreeNodePainter _right;
		private int _x;
		private int _y; 
		private int _value;

		public int Value
		{
			get
			{
				return _value;
			}

			set
			{
				_value = value;
			}
		}

		internal SearchTreeNodePainter Left
		{
			get
			{
				return _left;
			}

			set
			{
				_left = value;
			}
		}

		internal SearchTreeNodePainter Right
		{
			get
			{
				return _right;
			}

			set
			{
				_right = value;
			}
		}

		public int X
		{
			get
			{
				return _x;
			}

			set
			{
				_x = value;
			}
		}

		public int Y
		{
			get
			{
				return _y;
			}

			set
			{
				_y = value;
			}
		}
	}
}
