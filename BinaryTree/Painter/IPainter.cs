﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.Painter
{
	public interface IPainter
	{
		void Draw(Graphics g);
	}
}
