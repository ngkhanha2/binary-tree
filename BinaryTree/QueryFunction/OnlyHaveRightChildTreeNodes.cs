﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public class OnlyHaveRightChildTreeNodes : CountFunction
	{
		protected override bool CheckNode(SearchTreeNode node)
		{
			return node.Left == null && node.Right != null;
		}
	}
}