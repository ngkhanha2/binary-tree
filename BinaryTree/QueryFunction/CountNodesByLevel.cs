﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public class CountNodesByLevel : IQueryFunction
	{
		private int _level;

		public CountNodesByLevel(int level)
		{
			this._level = level;
		}

		public QueryResult Query(SearchTreeNode node)
		{
			Stack<Tuple<SearchTreeNode, int>> stackTuple = new Stack<Tuple<SearchTreeNode, int>>();
			int count = 0;
			if (node != null)
			{
				stackTuple.Push(new Tuple<SearchTreeNode, int>(node, 0));
				while (stackTuple.Count > 0)
				{
					Tuple<SearchTreeNode, int> t = stackTuple.Pop();
					if (t.Item2 == this._level)
					{
						++count;
					}
					else
					{
						if (t.Item1.Left != null)
						{
							stackTuple.Push(new Tuple<SearchTreeNode, int>(t.Item1.Left, t.Item2 + 1));
						}
						if (t.Item1.Right != null)
						{
							stackTuple.Push(new Tuple<SearchTreeNode, int>(t.Item1.Right, t.Item2 + 1));
						}
					}
				}
			}
			return new QueryResult(count);
		}
	}
}
