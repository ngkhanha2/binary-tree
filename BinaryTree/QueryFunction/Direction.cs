﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public class Direction : IQueryFunction
	{
		private int _key;
		public Direction(int key)
		{
			this._key = key;
		}

		public QueryResult Query(SearchTreeNode node)
		{
			int count = 0;
			var i = node;
			while (i != null)
			{
				if (i.Value == this._key)
				{
					return new QueryResult(count);
				}
				else if (_key < i.Value)
				{
					i = i.Left;
				}
				else
				{
					i = i.Right;
				}
				++count;
			}
			return new QueryResult(-1);
		}
	}
}
