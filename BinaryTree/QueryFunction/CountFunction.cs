﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public abstract class CountFunction : IQueryFunction
	{
		public QueryResult Query(SearchTreeNode node)
		{
			int count = 0;
			if (node != null)
			{
				Stack<SearchTreeNode> stackIterator = new Stack<SearchTreeNode>();
				stackIterator.Push(node);

				while (stackIterator.Count > 0)
				{
					SearchTreeNode i = stackIterator.Pop();
					if (CheckNode(i))
					{
						++count;
					}

					if (i.Left != null)
					{
						stackIterator.Push(i.Left);
					}
					if (i.Right != null)
					{
						stackIterator.Push(i.Right);
					}
				}
			}
			return new QueryResult(count);
		}

		protected abstract bool CheckNode(SearchTreeNode node);
	}
}
