﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.QueryFunction
{
	public class TreeHeight : IQueryFunction
	{
		public QueryResult Query(SearchTreeNode node)
		{
			Stack<Tuple<SearchTreeNode, int>> stackTuple = new Stack<Tuple<SearchTreeNode, int>>();
			int maxHeight = 0;
			if (node != null)
			{
				stackTuple.Push(new Tuple<SearchTreeNode, int>(node, 1));
				while (stackTuple.Count > 0)
				{
					Tuple<SearchTreeNode, int> t = stackTuple.Pop();
					if (t.Item2 > maxHeight)
					{
						maxHeight = t.Item2;
					}
					if (t.Item1.Left != null)
					{
						stackTuple.Push(new Tuple<SearchTreeNode, int>(t.Item1.Left, t.Item2 + 1));
					}
					if (t.Item1.Right != null)
					{
						stackTuple.Push(new Tuple<SearchTreeNode, int>(t.Item1.Right, t.Item2 + 1));
					}
				}
			}
			return new QueryResult(maxHeight);
		}
	}
}
