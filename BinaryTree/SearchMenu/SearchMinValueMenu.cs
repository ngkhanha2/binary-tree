﻿using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchMenu
{
	public class SearchMinValueMenu : SearchMenu
	{

		public SearchMinValueMenu(SearchTree tree, ISearchMenuProcessor processor) : base(tree, processor, "Tìm giá trị nhỏ nhất")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			var result = this.Tree.Search(new SearchMinValue());
			this.Processor.Process(result);
		}
	}
}
