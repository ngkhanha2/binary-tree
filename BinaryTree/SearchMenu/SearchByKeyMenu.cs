﻿using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchMenu
{
	public class SearchByKeyMenu : SearchMenu
	{
		public SearchByKeyMenu(SearchTree tree, ISearchMenuProcessor processor) : base(tree, processor, "Tìm theo khóa")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			var formInput = new FormInput("Nhập giá trị cần tìm");
			formInput.ShowDialog();
			if (formInput.Submited && this.Tree != null)
			{
				var result = this.Tree.Search(new SearchKey(formInput.Value));
				this.Processor.Process(result);
			}
		}
	}
}
