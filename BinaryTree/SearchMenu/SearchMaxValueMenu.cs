﻿using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchMenu
{
	public class SearchMaxValueMenu : SearchMenu
	{

		public SearchMaxValueMenu(SearchTree tree, ISearchMenuProcessor processor) : base(tree, processor, "Tìm giá trị lớn nhất")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			var result = this.Tree.Search(new SearchMaxValue());
			this.Processor.Process(result);
		}
	}
}
