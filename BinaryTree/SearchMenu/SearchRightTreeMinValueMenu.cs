﻿using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchMenu
{
	class SearchRightTreeMinValueMenu : SearchMenu
	{
		public SearchRightTreeMinValueMenu(SearchTree tree, ISearchMenuProcessor processor) : base(tree, processor, "Tìm giá trị nhỏ nhất trên cây con phải")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			var result = this.Tree.Search(new SearchRightTreeMinValue());
			this.Processor.Process(result);
		}
	}
}
