﻿using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchMenu
{
	class SearchLeftTreeMaxValueMenu : SearchMenu
	{
		public SearchLeftTreeMaxValueMenu(SearchTree tree, ISearchMenuProcessor processor) : base(tree, processor, "Tìm giá trị lớn nhất trên cây con trái")
		{
		}

		protected override void menu_Click(object sender, EventArgs e)
		{
			var result = this.Tree.Search(new SearchLeftTreeMaxValue());
			this.Processor.Process(result);
		}
	}
}
