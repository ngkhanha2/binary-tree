﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryTree.SearchMenu
{
	public abstract class SearchMenu : ProcessTreeMenu
	{
		private ISearchMenuProcessor _processor;

		public SearchMenu(SearchTree tree, ISearchMenuProcessor processor, string text) : base(tree, text)
		{
			_processor = processor;
		}

		public ISearchMenuProcessor Processor
		{
			get
			{
				return _processor;
			}
		}
	}
}
