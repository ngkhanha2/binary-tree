﻿using BinaryTree.SearchFunction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree.SearchMenu
{
	public interface ISearchMenuProcessor
	{
		void Process(SearchResult searchResult);
	}
}
