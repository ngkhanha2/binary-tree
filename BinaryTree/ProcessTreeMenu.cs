﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
	public abstract class ProcessTreeMenu : Menu
	{
		private SearchTree _tree;

		public ProcessTreeMenu(SearchTree tree, string text) : base(text)
		{
			this._tree = tree;
		}

		public SearchTree Tree
		{
			get
			{
				return _tree;
			}
		}
	}
}
