﻿using BinaryTree.SearchMenu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BinaryTree.SearchFunction;

namespace BinaryTree
{
	public class FormMainSearchProcessor : ISearchMenuProcessor
	{
		private FormMain _formMain;

		protected FormMainSearchProcessor(FormMain formMain)
		{
			this._formMain = formMain;
		}

		public void Process(SearchResult searchResult)
		{
			if (searchResult != null)
			{
				_formMain.ResetSearchResultList();
				SearchTreeNode node = (SearchTreeNode)searchResult.Value;
				_formMain.AddSearchResult(node.Value, System.Drawing.Color.Red);
				_formMain.ReDraw();
			}
		}

		private static FormMainSearchProcessor formMainInstance;

		public static FormMainSearchProcessor GetInstance(FormMain formMain)
		{
			if (formMainInstance == null)
			{
				formMainInstance = new FormMainSearchProcessor(formMain);
			}
			return formMainInstance;
		}
	}
}
