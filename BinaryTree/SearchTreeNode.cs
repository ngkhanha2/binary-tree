﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BinaryTree
{
	public abstract class SearchTreeNode
	{
		public virtual SearchTreeNode Left
		{
			get
			{
				return null;
			}
		}

		public virtual SearchTreeNode Right
		{
			get
			{
				return null;
			}
		}

		public virtual int Value
		{
			get
			{
				return 0;
			}
		}
	}
}
