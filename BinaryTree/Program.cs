﻿using BinaryTree.Traversal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryTree
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new FormMain());

			//Console.WriteLine("Left - Node - Right Traversal");
			//PrintList(tree.Travel(new LeftNodeRight()));

			//Console.WriteLine("Left - Right - Node Traversal");
			//PrintList(tree.Travel(new LeftRightNode()));

			//Console.WriteLine("Node - Left - Right Traversal");
			//PrintList(tree.Travel(new NodeLeftRight()));

			//SearchTreeNode i = (SearchTreeNode)(tree.Search(new SearchFunction.SearchKey(6))).Value;
			//Console.WriteLine(i.Value);
			//int v = (int)(tree.Query(new QueryFunction.CountFunction(new LeapNodeCondition()))).Value;
			//Console.WriteLine(v);
			//Console.ReadLine();

		}

		static void PrintList(List<SearchTreeNode> l)
		{
			foreach (SearchTreeNode n in l)
			{
				Console.Write(n.Value.ToString() + " ");
			}
			Console.WriteLine();
		}
	}
}
