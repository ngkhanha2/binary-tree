﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BinaryTree
{
	public abstract class Menu
	{
		private ToolStripMenuItem _menu;

		public Menu(string text)
		{
			_menu = new ToolStripMenuItem(text);
			_menu.Click += menu_Click;
		}

		protected abstract void menu_Click(object sender, EventArgs e);

		public ToolStripMenuItem ToolStripMenu
		{
			get
			{
				return _menu;
			}
		}
	}
}
