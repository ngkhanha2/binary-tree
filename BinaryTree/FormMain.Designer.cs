﻿namespace BinaryTree
{
	partial class FormMain
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormMain));
			this.toolStrip1 = new System.Windows.Forms.ToolStrip();
			this.btnAddKey = new System.Windows.Forms.ToolStripButton();
			this.btnDeleteKey = new System.Windows.Forms.ToolStripButton();
			this.ddTraversal = new System.Windows.Forms.ToolStripDropDownButton();
			this.ddQuery = new System.Windows.Forms.ToolStripDropDownButton();
			this.ddSearch = new System.Windows.Forms.ToolStripDropDownButton();
			this.statusStrip1 = new System.Windows.Forms.StatusStrip();
			this.panelTree = new System.Windows.Forms.Panel();
			this.toolStrip1.SuspendLayout();
			this.SuspendLayout();
			// 
			// toolStrip1
			// 
			this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAddKey,
            this.btnDeleteKey,
            this.ddTraversal,
            this.ddQuery,
            this.ddSearch});
			this.toolStrip1.Location = new System.Drawing.Point(0, 0);
			this.toolStrip1.Name = "toolStrip1";
			this.toolStrip1.Size = new System.Drawing.Size(584, 25);
			this.toolStrip1.TabIndex = 0;
			this.toolStrip1.Text = "toolStrip1";
			// 
			// btnAddKey
			// 
			this.btnAddKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnAddKey.Image = ((System.Drawing.Image)(resources.GetObject("btnAddKey.Image")));
			this.btnAddKey.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnAddKey.Name = "btnAddKey";
			this.btnAddKey.Size = new System.Drawing.Size(75, 22);
			this.btnAddKey.Text = "Thêm giá trị";
			this.btnAddKey.Click += new System.EventHandler(this.btnAddKey_Click);
			// 
			// btnDeleteKey
			// 
			this.btnDeleteKey.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.btnDeleteKey.Image = ((System.Drawing.Image)(resources.GetObject("btnDeleteKey.Image")));
			this.btnDeleteKey.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.btnDeleteKey.Name = "btnDeleteKey";
			this.btnDeleteKey.Size = new System.Drawing.Size(64, 22);
			this.btnDeleteKey.Text = "Xóa giá trị";
			this.btnDeleteKey.Click += new System.EventHandler(this.btnDeleteKey_Click);
			// 
			// ddTraversal
			// 
			this.ddTraversal.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.ddTraversal.Image = ((System.Drawing.Image)(resources.GetObject("ddTraversal.Image")));
			this.ddTraversal.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ddTraversal.Name = "ddTraversal";
			this.ddTraversal.Size = new System.Drawing.Size(72, 22);
			this.ddTraversal.Text = "Duyệt cây";
			// 
			// ddQuery
			// 
			this.ddQuery.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.ddQuery.Image = ((System.Drawing.Image)(resources.GetObject("ddQuery.Image")));
			this.ddQuery.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ddQuery.Name = "ddQuery";
			this.ddQuery.Size = new System.Drawing.Size(65, 22);
			this.ddQuery.Text = "Truy vấn";
			// 
			// ddSearch
			// 
			this.ddSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
			this.ddSearch.Image = ((System.Drawing.Image)(resources.GetObject("ddSearch.Image")));
			this.ddSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
			this.ddSearch.Name = "ddSearch";
			this.ddSearch.Size = new System.Drawing.Size(70, 22);
			this.ddSearch.Text = "Tìm kiếm";
			// 
			// statusStrip1
			// 
			this.statusStrip1.Location = new System.Drawing.Point(0, 339);
			this.statusStrip1.Name = "statusStrip1";
			this.statusStrip1.Size = new System.Drawing.Size(584, 22);
			this.statusStrip1.TabIndex = 1;
			this.statusStrip1.Text = "statusStrip1";
			// 
			// panelTree
			// 
			this.panelTree.BackColor = System.Drawing.Color.White;
			this.panelTree.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
			this.panelTree.Dock = System.Windows.Forms.DockStyle.Fill;
			this.panelTree.Location = new System.Drawing.Point(0, 25);
			this.panelTree.Name = "panelTree";
			this.panelTree.Size = new System.Drawing.Size(584, 314);
			this.panelTree.TabIndex = 2;
			this.panelTree.SizeChanged += new System.EventHandler(this.panelTree_SizeChanged);
			this.panelTree.Paint += new System.Windows.Forms.PaintEventHandler(this.panelTree_Paint);
			// 
			// FormMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(584, 361);
			this.Controls.Add(this.panelTree);
			this.Controls.Add(this.statusStrip1);
			this.Controls.Add(this.toolStrip1);
			this.Name = "FormMain";
			this.Text = "Form1";
			this.Load += new System.EventHandler(this.FormMain_Load);
			this.toolStrip1.ResumeLayout(false);
			this.toolStrip1.PerformLayout();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip toolStrip1;
		private System.Windows.Forms.ToolStripButton btnAddKey;
		private System.Windows.Forms.ToolStripButton btnDeleteKey;
		private System.Windows.Forms.ToolStripDropDownButton ddTraversal;
		private System.Windows.Forms.ToolStripDropDownButton ddSearch;
		private System.Windows.Forms.StatusStrip statusStrip1;
		private System.Windows.Forms.Panel panelTree;
		private System.Windows.Forms.ToolStripDropDownButton ddQuery;
	}
}

